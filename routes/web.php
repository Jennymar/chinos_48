<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//primera ruta de get 
Route::get('hola', function(){ 
   echo"Hola estoy en laravel";
});

//primera ruta de get 
Route::get('arreglo', function(){ 
    //crear arreglo estudiantes
    $estudiantes = ["AD" => "Andres",
                    "LR" => "Laura", 
                    "RD" => "Rodrigo"];
    //var_dump($estudiantes);

    //recorrer un arreglo
    foreach($estudiantes as $indice => $estudiante){
         echo "$estudiante tiene indice $indice <hr />";
    }
 });


Route::get('paises', function(){ 
    //crear un arreglo con información de paises
   $paises = [
       "Colombia" => [
                       "capital" => "Bogotá",
                       "moneda" => "Peso",
                       "poblacion" => 50.372 
                     ],
       "Ecuador" => [
                       "capital" => "Quito",
                       "moneda" => "Dolar",
                       "poblacion" => 17.517
                    ],
       "Brasil" => [
                       "capital" => "Brasilia",
                       "moneda" => "Real brasileño",
                       "poblacion" => 212.216   
                   ],
       "Bolivia" => [
                       "capital" => "Sucre",
                       "moneda" => "Boliviano",
                       "poblacion" => 11.633 
    ],
   ];
 
  /* echo "<pre>";
   var_dump($paises);
   echo "</prev>"; */

   //Recorrer la primera dimension del arrglo
 /*  foreach ($paises as $pais => $infopais) {
       echo "<h2> $pais </h2>";
       echo "capital:" . $infopais["capital"] . "<br />";
       echo "moneda:" . $infopais["moneda"] . "<br />";
       echo "poblacion(en millones de habitantes):" . $infopais["poblacion"];
       echo "<hr />";
    } */

//Mostrar una vista para presentar los paises 
//En MVC puedo pasar datos a una vista
return view('paises')->with("paises" , $paises);


 });
